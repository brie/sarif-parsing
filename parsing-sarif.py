"""
# Parsing SARIF

This is a **quick** script designed to parse [SARIF](https://docs.oasis-open.org/sarif/sarif/v2.1.0/sarif-v2.1.0.html) files. I focus on the `SARIF` files created by `semgrep`.

--
[Brie 🌈🦄](https://brie.dev)
"""
import json
import loguru

def parse_a_result(result):
    """
    Parse each result individually. We are totally focused on the suppressions property.
    """
    number_of_suppressed_findings = int()
    number_of_suppressed_findings = 0
    problematic_loc = result['locations'][0]['physicalLocation']['region']['snippet']['text']
    message = result.get('message', '')
    loguru.logger.info("Here's the problematic snippet:")
    loguru.logger.info(problematic_loc)
    loguru.logger.info("Here's why we think that is so bad...")
    loguru.logger.info(message['text'])
    suppressions = result.get('suppressions', [])
    if result.get('ruleid'):
        loguru.logger.debug("📏 Rule ID: {}", result.get('ruleid'))
    location = result['locations']
    if suppressions:
        # EXPLORE
        for suppression in suppressions:
            number_of_suppressed_findings = number_of_suppressed_findings + 1
            kind_of_suppression = suppression['kind']
            loguru.logger.info("✨ There is a suppression of kind '{}', btw.", kind_of_suppression)
            justification = suppression.get('justification', '')
            if justification:
                loguru.logger.debug("🧑‍⚖️ Justification: {}", justification)
            else:
                loguru.logger.info("👋 There is no 'Justification' for this one. Just FYI.")
    else:
        loguru.logger.success("😎 NO SUPPRESSIONS detected for this one. On to the next...")
    return number_of_suppressed_findings

def get_run_info(run):
    """
    Get some metadata about the invocations in this particular run
    """
    number_of_invocations = int()
    number_of_invocations = 0
    for invocation in run['invocations']:
        number_of_invocations = number_of_invocations + 1    
        if invocation['executionSuccessful']:
            loguru.logger.info("🎉 A SUCCESSFUL invocation of semgrep!")        
        if not invocation['executionSuccessful']:
            loguru.logger.info("😭 A FAILED invocation of semgrep!")        
    loguru.logger.success("🚀 NUMBER OF INVOCATIONS DETECTED: {}", number_of_invocations)

def parse_sarif_for_results(sarif_file_path):
    with open(sarif_file_path, 'r') as file:
        sarif_data = json.load(file)
        runs = sarif_data.get('runs', [])
        if len(runs) == 1:
            loguru.logger.success("🐈‍⬛ We have a SARIF file with data about a single run.")
        elif len(runs) >= 2:
            loguru.logger.success("🐈‍⬛🐈‍⬛ We have a SARIF file with data about {} runs.", len(runs))
        else:
            loguru.logger.error("😭 Why are you here?")
        for run in runs:
            get_run_info(run)
            number_of_results = int()
            number_of_results = 0
            total_suppressions = int()
            total_suppressions = 0
            results = run.get('results', [])
            for result in results:
                number_of_results = number_of_results + 1
                new_suppressions = parse_a_result(result)
                total_suppressions = total_suppressions + new_suppressions
            loguru.logger.info("🚀 NUMBER OF RESULTS PARSED: {}. 😅", number_of_results)
            loguru.logger.info("ℹ️ Of those {} findings, {} were suppressed.", number_of_results, total_suppressions)

if __name__ == "__main__":
    sarif_file = 'howdy.sarif'
    parse_sarif_for_results(sarif_file)